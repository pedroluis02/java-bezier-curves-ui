package com.github.pedroluis02.jglbeziercurves;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JFrame;

/**
 * Bezier
 * @author Pedro Luis
 */
public class MainFrame extends JFrame {

    private ScenePanel escena;

    @SuppressWarnings("static-access")
    public MainFrame(String title) {
        super(title);
        escena = new ScenePanel();
        Box box = Box.createHorizontalBox();
        box.setBorder(BorderFactory.createLoweredBevelBorder());
        box.add(escena, Box.CENTER_ALIGNMENT);
        this.add(box, BorderLayout.CENTER);
        this.add(escena.getPanel(), BorderLayout.WEST);
    }

    static Dimension getEscreen() {
        return Toolkit.getDefaultToolkit().getScreenSize();
    }

    public static void main(String[] args) {
        MainFrame frame = new MainFrame("Bezier Curves");
        frame.setSize(getEscreen().width / 2, getEscreen().height / 2);
        frame.setLocation(getEscreen().width / 4, getEscreen().height / 4);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

}
