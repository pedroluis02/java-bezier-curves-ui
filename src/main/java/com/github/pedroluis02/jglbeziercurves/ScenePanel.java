package com.github.pedroluis02.jglbeziercurves;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.gl2.GLUT;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import java.awt.Font;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.awt.event.MouseListener;

import javax.swing.JPanel;
import javax.swing.JButton;

/**
 * ScenePanel
 * @author Pedro Luis
 */
public class ScenePanel extends GLCanvas implements GLEventListener {

    private GL2 gl;
    private GLU glu;
    private GLUT glut;

    private int widht, heigth, NRO;
    private float CARTESIANA;
    private boolean PAINTCURVA;

    private Bezier bezier = new Bezier();
    private ArrayList<Point3D> curva = new ArrayList<Point3D>();

    private JPanel panel;
    private JButton Bborrar;
    private JButton Bdeshacer;
    private JButton Bgraficar;
    private JButton Blimpiar;

    private PopupMenu menu;

    public ScenePanel() {
        this.addGLEventListener(this);
        this.addMouseListener(manejarMouse());
        this.addKeyListener(this.keyboard());
        this.add(this.menuContex());
        this.glu = new GLU();
        this.glut = new GLUT();
        this.widht = 700;
        this.heigth = 700;
        this.NRO = 0;
        this.CARTESIANA = 50.f;
        this.PAINTCURVA = false;
    }

    @Override
    public void init(GLAutoDrawable glad) {
        gl = glad.getGL().getGL2();
        //gl.glClearColor(0.85f, 0.95f, 0.95f, 1f);
        gl.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    }

    @Override
    public void display(GLAutoDrawable glad) {
        gl = glad.getGL().getGL2();
        gl.glClear(GL2.GL_COLOR_BUFFER_BIT);
        gl.glClear(GL2.GL_DEPTH_BUFFER_BIT);
        if (bezier.numPoints() > 0) {
            this.paintPoints(gl);
        }
        if (PAINTCURVA) {
            this.pintarCurva(gl);
        }
        gl.glFlush();
    }

    @Override
    public void reshape(GLAutoDrawable glad, int x, int y, int width, int heigth) {
        gl = glad.getGL().getGL2();
        this.widht = width;
        this.heigth = heigth;
        gl.glOrtho(0, CARTESIANA, 0, CARTESIANA, 0, CARTESIANA);
        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glLoadIdentity();
        gl.glViewport(x, y, width, heigth);
    }

    @Override
    public void dispose(GLAutoDrawable arg0) {
    }

    private Point3D coordScreen(Point3D p) {
        int cx = widht / (int) CARTESIANA, cy = heigth / (int) CARTESIANA;
        p.x = p.x - p.x % cx;
        p.y = p.y - p.y % cy;

        for (int i = 0; i <= widht; i++) {
            for (int j = 0; j <= heigth; j++) {
                if (p.x == i && p.y == j) {
                    return new Point3D(j / cy, i / cx, 0.f);
                }
            }
        }
        return new Point3D(-1, -1, 0);
    }

    private Point3D coordCartesiana(Point3D p) {
        if (coordScreen(p) == new Point3D(-1, -1, 0)) {
            return new Point3D(-1, -1, 0);
        } else {
            int CAR = (int) (CARTESIANA / 2) - 1;
            Point3D p1 = coordScreen(p);
            if (p.x < CAR) {
                return new Point3D(p1.y, p1.x + 2 * (CAR - p1.x) + 1, 0);
            } else if (p.x > CAR) {
                return new Point3D(p1.y, p1.x - 2 * (p1.x - CAR) + 1, 0);
            } else if (p.x == CAR) {
                return new Point3D(p1.y, p1.x, 0);
            }
            return new Point3D(-1, -1, 0);
        }
    }

    private void procesar() {
        if (curva.size() == 0 && bezier.numPoints() > 0) {
            curva = bezier.bezier();
            NRO = bezier.numPoints();
        } else if (NRO != bezier.numPoints()) {
            curva.clear();
            bezier.clearCurva();
            curva = bezier.bezier();
            NRO = bezier.numPoints();
        }
    }

    private void paintPoints(GL2 gl) {
        this.gl = gl;
        int n = bezier.numPoints();
        if (n == 0) {
            return;
        }
        gl.glPointSize(10.f);
        gl.glColor3f(1, 0, 0);
        gl.glBegin(GL2.GL_POINTS);
        for (int i = 0; i < n; i++) {
            Point3D p = bezier.getPoint(i);
            gl.glVertex3f(p.x, p.y, p.z);
        }
        gl.glEnd();

        gl.glBegin(GL2.GL_LINE_STRIP);
        gl.glColor3f(1, 0, 0);
        for (int i = 0; i < n; i++) {
            Point3D p = bezier.getPoint(i);
            gl.glVertex3f(p.x, p.y, p.z);
        }
        gl.glEnd();
    }

    void pintarCurva(GL2 gl) {
        this.gl = gl;
        int n = curva.size();
        if (n == 0) {
            return;
        }
        gl.glPointSize(5.f);
        gl.glBegin(GL2.GL_POINTS);
        gl.glColor3f(0, 0, 1);
        for (int i = 0; i < n; i++) {
            Point3D p = curva.get(i);
            gl.glVertex3f(p.x, p.y, p.z);
        }
        gl.glEnd();
    }

    private MouseListener manejarMouse() {
        return new MouseListener() {

            public void mouseClicked(MouseEvent e) {
            }

            public void mousePressed(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON1) {
                    Point3D p = coordCartesiana(new Point3D(e.getX(), e.getY(), 0));
                    bezier.addPoint(p);
                    repaint();
                }
                showMenuContext(e);
            }

            public void mouseReleased(MouseEvent e) {
                showMenuContext(e);
            }

            public void mouseEntered(MouseEvent e) {
            }

            public void mouseExited(MouseEvent e) {
            }

        };
    }

    private KeyListener keyboard() {
        return new KeyListener() {

            public void keyTyped(KeyEvent e) {
            }

            public void keyPressed(KeyEvent e) {
                switch (e.getKeyCode()) {
                    case 27:
                        System.exit(0);
                        break;
                    case KeyEvent.VK_G:
                        menuItems(1);
                        break;
                    case KeyEvent.VK_L:
                        menuItems(2);
                        break;
                    case KeyEvent.VK_D:
                        menuItems(3);
                        break;
                    case KeyEvent.VK_B:
                        menuItems(4);
                        break;
                }
            }

            public void keyReleased(KeyEvent e) {
            }
        };
    }

    private void menuItems(int option) {
        switch (option) {
            //grafica la curva
            case 1:
                procesar();
                PAINTCURVA = true;
                break;
            //oculta la curva
            case 2:
                PAINTCURVA = false;
                break;
            //Deshcer ultimo punto ingresado
            case 3:
                if (bezier.numPoints() == 1) {
                    bezier.removePoint();
                    curva.clear();
                    bezier.clear();
                    break;
                } else if (bezier.numPoints() > 1) {
                    bezier.removePoint();
                    procesar();
                    break;
                }
                break;
            //elimina los puntos de la curva
            case 4:
                curva.clear();
                bezier.clear();
                PAINTCURVA = false;
                NRO = 0;
                break;
        }
        repaint();
    }

    public JPanel getPanel() {
        this.panel = new JPanel();
        initComponents(panel);
        return panel;
    }

    private void initComponents(JPanel panel) {

        Bdeshacer = new javax.swing.JButton();
        Bgraficar = new javax.swing.JButton();
        Blimpiar = new javax.swing.JButton();
        Bborrar = new javax.swing.JButton();

        Bdeshacer.setText("Deshacer");
        Bdeshacer.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        Bdeshacer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BdeshacerActionPerformed(evt);
            }
        });

        Bgraficar.setText("Graficar");
        Bgraficar.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        Bgraficar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BgraficarActionPerformed(evt);
            }
        });

        Blimpiar.setText("Limpiar");
        Blimpiar.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        Blimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BlimpiarActionPerformed(evt);
            }
        });

        Bborrar.setText("Borrar");
        Bborrar.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        Bborrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BborrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(panel);
        panel.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Bgraficar, javax.swing.GroupLayout.DEFAULT_SIZE, 88, Short.MAX_VALUE)
                                        .addComponent(Bborrar, javax.swing.GroupLayout.DEFAULT_SIZE, 88, Short.MAX_VALUE)
                                        .addComponent(Bdeshacer, javax.swing.GroupLayout.DEFAULT_SIZE, 88, Short.MAX_VALUE))
                                .addContainerGap())
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addContainerGap()
                                        .addComponent(Blimpiar, javax.swing.GroupLayout.DEFAULT_SIZE, 88, Short.MAX_VALUE)
                                        .addContainerGap()))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(42, 42, 42)
                                .addComponent(Bgraficar, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(104, 104, 104)
                                .addComponent(Bdeshacer, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(33, 33, 33)
                                .addComponent(Bborrar, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(51, Short.MAX_VALUE))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(107, 107, 107)
                                        .addComponent(Blimpiar, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addContainerGap(179, Short.MAX_VALUE)))
        );
    }

    private void BgraficarActionPerformed(java.awt.event.ActionEvent evt) {
        menuItems(1);
    }

    private void BlimpiarActionPerformed(java.awt.event.ActionEvent evt) {
        menuItems(2);
    }

    private void BdeshacerActionPerformed(java.awt.event.ActionEvent evt) {
        menuItems(3);
    }

    private void BborrarActionPerformed(java.awt.event.ActionEvent evt) {
        menuItems(4);
    }

    PopupMenu menuContex() {
        menu = new PopupMenu();
        MenuItem mi[] = new MenuItem[4];
        mi[0] = new MenuItem("Graficar");
        mi[0].setActionCommand("graficar");
        mi[1] = new MenuItem("Limpiar");
        mi[1].setActionCommand("limpiar");
        mi[2] = new MenuItem("Deshacer");
        mi[2].setActionCommand("deshacer");
        mi[3] = new MenuItem("Borrar");
        mi[3].setActionCommand("borrar");
        menu.add(mi[0]);
        menu.add(mi[1]);
        menu.add(mi[2]);
        menu.add(mi[3]);
        menu.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 12));
        menu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (e.getActionCommand().compareTo("graficar") == 0) {
                    menuItems(1);
                }
                if (e.getActionCommand().compareTo("limpiar") == 0) {
                    menuItems(2);
                }
                if (e.getActionCommand().compareTo("deshacer") == 0) {
                    menuItems(3);
                }
                if (e.getActionCommand().compareTo("borrar") == 0) {
                    menuItems(4);
                }
            }
        });
        return menu;
    }

    void showMenuContext(MouseEvent e) {
        if (e.isPopupTrigger()) {
            menu.show(e.getComponent(), e.getX(), e.getY());
        }
    }
}
