package com.github.pedroluis02.jglbeziercurves;

import java.util.ArrayList;

/**
 * Bezier
 * @author Pedro Luis
 */
public class Bezier {

    private ArrayList<Point3D> puntos;
    private ArrayList<Point3D> curva;

    public Bezier() {
        puntos = new ArrayList<Point3D>();
        curva = new ArrayList<Point3D>();
    }

    public void addPoint(Point3D pto) {
        puntos.add(pto);
    }

    public Point3D getPoint(int pos) {
        return puntos.get(pos);
    }

    public void removePoint() {
        puntos.remove(puntos.size() - 1);
    }

    public int numPoints() {
        return puntos.size();
    }

    public int numCurva() {
        return curva.size();
    }

    public Point3D getCurva(int pos) {
        return curva.get(pos);
    }

    public void clearCurva() {
        curva.clear();
    }

    public void clear() {
        puntos.clear();
        curva.clear();
    }

    public ArrayList<Point3D> bezier() {
        /*float t = 0, x = 0, y = 0, z = 0;
	int n = puntos.size();

	if(n <= 1)
	{
            curva.add(puntos.get(0));
	    return curva;
        }
	for(float p=0.00f; p<=1; p +=0.001f)
	{
		x = 0;
		y = 0;
		for(int i=0; i<n; i++)
		{
		    Point3D pto = puntos.get(i);
		    t = binomial(n-1,i,p);
		    x += pto.getX() * t;
				y += pto.getY() * t;
                    y += pto.getZ() * t;
		}
		curva.add(new Point3D(x,y,z));
	}
	return curva;*/
        return curva = Bernstein.calcular(puntos);
    }

    private int taylor(int n, int i) {
        int k = 0, j = 0;
        int t[][] = new int[n + 1][];

        for (; k < n; k++) {
            t[i] = new int[n + 1];
        }

        t[0][0] = 1;
        t[1][0] = 1;
        t[1][1] = 1;

        if (n < 2) {
            return t[n][i];
        }

        for (k = 2; k <= n; k++) {
            t[k][0] = 1;
            for (j = 1; j < k; j++) {
                t[k][j] = t[k - 1][j - 1] + t[k - 1][j];
            }
            t[k][j] = 1;
        }
        return t[n][i];
    }

    private float binomial(int n, int i, float p) {
        float v = taylor(n, i) * Bernstein.exp(p, i) * Bernstein.exp(1 - p, n - i);
        return v;
    }
}
