package com.github.pedroluis02.jglbeziercurves;

/**
 * Bernstein
 * @author Pedro Luis
 */
import java.util.ArrayList;

public class Bernstein {

    public static ArrayList<Point3D> calcular(ArrayList<Point3D> puntos) {
        ArrayList<Point3D> curva = new ArrayList<>();
        int n = puntos.size();
        if (n <= 1) {
            curva.add(puntos.get(0));
            return curva;
        }
        for (float t = 0.00f; t <= 1; t += 0.001f) {
            float x = 0.f;
            float y = 0.f;
            float z = 0.f;
            for (int i = 0; i < n; i++) {
                Point3D pto = puntos.get(i);
                float p = pol(i, n - 1) * exp(1 - t, (n - 1) - i) * exp(t, i);
                x += pto.getX() * (float) p;
                y += pto.getY() * (float) p;
                z += pto.getZ() * (float) p;
            }
            curva.add(new Point3D(x, y, z));
        }
        return curva;
    }

    static float pol(int i, int n) {
        return fact(n) / (fact(i) * fact(n - i));
    }

    static float fact(float n) {
        if (n < 2) {
            return 1.0f;
        } else {
            return n * fact(n - 1);
        }
    }

    static float exp(float base, int exp) {
        float r = 1.0f;
        int i = 0;

        for (; i < exp; i++) {
            r *= base;
        }
        return r;
    }
}
